const express = require('express');
const app = express();
const request = require('supertest');

describe('Rest API server', () => {
  const httpVerbs = ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'];

  beforeEach(() => {
    const mockResponseBody = { name: 'john' };
    httpVerbs.map(httpVerb => {
      const expressMethod = httpVerb.toLowerCase();

      app[expressMethod]('/user', function (req, res) {
        res.status(200).json(mockResponseBody);
      });
    });
  });

  httpVerbs.map(httpVerb => {
    it(`should handle ${httpVerb} request`, done => {
      const expressMethod = httpVerb.toLowerCase();

      request(app)[expressMethod]('/user')
        .expect(200)
        .expect('Content-Type', /json/)
        .expect('Content-Length', '15')
        .end(function(err, res) {
          if (err) throw err;
          done();
        });
    });
  });
});